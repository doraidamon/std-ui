/*
 * std-ui - Building User Interfaces
 * by contributors
 */

#ifndef _STDUI_H_
#define _STDUI_H_

#include "stduiplatform.h"

#ifdef __cplusplus
extern "C" {
#endif

#define STDUI_FALSE 0
#define STDUI_TRUE 1

typedef struct STDUI_Library STDUI_Library;
typedef struct STDUI_Element STDUI_Element;
typedef struct STDUI_ControlFlags STDUI_ControlFlags;

typedef enum STDUI_CursorType STDUI_CursorType;
typedef struct STDUI_Window STDUI_Window;

struct STDUI_Library
{
    STDUI_Platform platform;
};

struct STDUI_ControlFlags
{
    // Control flags
    int contentEditable : 1;
    int focusable : 1;
};

enum STDUI_CursorType
{
    Normal,
    Pointer,
};

/// An element
struct STDUI_Element
{
    STDUI_Element** _children;
    STDUI_ControlFlags flags;
    STDUI_CursorType cursorType;
};

void STDUI_functionComponent(STDUI_Element (*fc)(void));

/// Window
struct STDUI_Window
{
    int width;
    int height;
};

STDUI_Window STDUI_createWindow(void);

#ifdef __cplusplus
}
#endif

#endif
