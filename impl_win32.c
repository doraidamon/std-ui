#include "impl_win32.h"

STDUI_Platform STDUI_initWin32Platform(void)
{
    STDUI_PlatformVtable vtable = {
        .createWindow = _STDUI_win32CreateWindow,
    };

    STDUI_Platform platform = {
        .vtable = vtable,
    };

    return platform;
}
