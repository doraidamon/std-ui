#ifndef _STDUIPLATFORM_H_
#define _STDUIPLATFORM_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct STDUI_PlatformVtable STDUI_PlatformVtable;
typedef struct STDUI_Platform STDUI_Platform;

typedef struct STDUI_PlatformWindowConfig STDUI_PlatformWindowConfig;

struct STDUI_PlatformWindowConfig
{
    int posX;
    int posY;
    int width;
    int height;
    const char* title;
};

struct STDUI_PlatformVtable
{
    void (*createWindow)(STDUI_PlatformWindowConfig);
};

struct STDUI_Platform
{
   STDUI_PlatformVtable vtable;
};

#ifdef __cplusplus
}
#endif


#endif
