#ifndef _STDUI_IMPL_WIN32_H_
#define _STDUI_IMPL_WIN32_H_

#include "stduiplatform.h"

void _STDUI_win32CreateWindow(STDUI_PlatformWindowConfig config);
STDUI_Platform STDUI_initWin32Platform(void);

#endif
